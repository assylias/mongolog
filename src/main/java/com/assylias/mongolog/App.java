package com.assylias.mongolog;

import java.nio.file.Paths;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Hello world!
 *
 */
public class App extends Application {

    public static void main(String... args) {
        launch(App.class);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Paths.get("./src/main/resources/fxml/main.fxml").toUri().toURL());
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
