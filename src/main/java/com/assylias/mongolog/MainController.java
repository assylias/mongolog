/*
 * Copyright (C) 2011 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.mongolog;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import org.controlsfx.dialog.Dialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Yann Le Tallec
 */
public class MainController {

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);
    private static final DateFormat DF = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");

    @FXML
    AnchorPane root;

    @FXML
    TextField host;
    @FXML
    ComboBox<String> database;
    @FXML
    ComboBox<String> collection;
    @FXML
    ListView<String> level;
    @FXML
    ListView<String> marker;
    @FXML
    TextField regex;
    @FXML
    TextField afterDate;
    @FXML
    TextField beforeDate;
    @FXML
    CheckBox exceptionsOnly;

    @FXML
    TableView<LogEntry> logTable;
    @FXML
    TableColumn<LogEntry, String> levelColumn;
    @FXML
    TableColumn<LogEntry, String> markerColumn;
    @FXML
    TableColumn<LogEntry, Date> dateColumn;
    @FXML
    TableColumn<LogEntry, String> messageColumn;

    @FXML
    TextArea details;
    @FXML
    Label status;
    @FXML
    ProgressIndicator progress;

    private MongoClient client;
    private DB db;
    private DBCollection coll;
    private final List<LogEntry> logs = new ArrayList<>();
    private ObservableList<String> selectedLevels;
    private ObservableList<String> selectedMarkers;
    private Pattern p;
    private final Predicate<LogEntry> filter = (LogEntry e) -> {
        String l = e.getLevel().get();
        l = (l == null ? "" : l);
        if (!selectedLevels.contains(l)) return false;
        String m = e.getMarker().get();
        m = (m == null ? "" : m);
        if (!selectedMarkers.contains(m)) return false;
        if (p.matcher(e.getMessage().get()).find() || p.matcher(e.getDetails().get()).find()) return true;
        return false;
    };
    private static final ExecutorService executor = Executors.newCachedThreadPool();

    @FXML
    public void initialize() {
        selectedLevels = level.getSelectionModel().getSelectedItems();
        selectedMarkers = marker.getSelectionModel().getSelectedItems();
        level.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        marker.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        levelColumn.setCellValueFactory((param) -> param.getValue().getLevel());
        markerColumn.setCellValueFactory((param) -> param.getValue().getMarker());
        dateColumn.setCellValueFactory((param) -> param.getValue().getDate());
        dateColumn.setCellFactory((TableColumn<LogEntry, Date> param) -> {
            return new TableCell<LogEntry, Date>() {
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : DF.format(item));
                }
            };
        });
        messageColumn.setCellValueFactory((param) -> param.getValue().getMessage());
        messageColumn.setCellFactory((TableColumn<LogEntry, String> param) -> {
            return new TableCell<LogEntry, String>() {
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? "" : item.split("(" + Pattern.quote("\r") + ")?" + Pattern.quote("\n"))[0]);
                }
            };
        });
        ReadOnlyObjectProperty<LogEntry> selected = logTable.getSelectionModel().selectedItemProperty();
        details.textProperty().bind(Bindings.when(selected.isNull()).then("").otherwise(selected.asString()));

        host.setOnAction((ActionEvent event) -> {
            LOG.debug("host set to " + host.getText());
            progress.setVisible(true);
            executor.submit(new FetchDatabasesTask());
        });
        database.setOnAction((ActionEvent event) -> {
            LOG.debug("DB set to " + database.getValue());
            progress.setVisible(true);
            executor.submit(new FetchCollectionsTask());
        });
        collection.setOnAction((ActionEvent event) -> {
            LOG.debug("Collection set to " + database.getValue());
            progress.setVisible(true);
            executor.submit(new FetchLogEventsTask());
        });
        selectedLevels.addListener((ListChangeListener.Change<? extends String> c) -> {
            refreshTable();
        });
        selectedMarkers.addListener((ListChangeListener.Change<? extends String> c) -> {
            refreshTable();
        });
        regex.setOnAction((ActionEvent event) -> {
             p = Pattern.compile(Pattern.quote(regex.getText()));
            refreshTable();
        });
        host.setText("192.168.3.12");
        host.requestFocus();
        p = Pattern.compile(Pattern.quote(regex.getText()));
        root.getScene().getWindow().setOnCloseRequest((WindowEvent event) -> {
            if (client != null) {
                LOG.debug("Closing Mongo connection");
                client.close();
            }
        });
    }

    private void refreshTable() {
        logTable.getItems().clear();
        logs.stream().filter(filter).collect(Collectors.toCollection(() -> logTable.getItems()));
        status.setText(logTable.getItems().size() + " records (out of " + logs.size() + ")");
    }

    private class FetchDatabasesTask extends MongoTask<Void> {

        public FetchDatabasesTask() {
            super("Error while fetching database names from Mongo", "Mongo Error", progress);
        }

        @Override protected Void call() throws Exception {
            if (client != null) client.close();
            client = new MongoClient(host.getText());
            List<String> dbs = client.getDatabaseNames();
            Platform.runLater(() -> {
                database.getItems().setAll(dbs);
                collection.getItems().clear();
            });
            return null;
        }

        @Override protected void failed() {
            super.failed();
            Platform.runLater(() -> {
                database.getItems().clear();
                collection.getItems().clear();
            });
        }
    }
    
    private class FetchCollectionsTask extends MongoTask<Void> {

        public FetchCollectionsTask() {
            super("Error while fecthing collections from Mongo", "Mongo Error", progress);
        }

        @Override protected Void call() throws Exception {
            LOG.debug("Fecthing collections");
            db = client.getDB(database.getValue());
            Set<String> colls = db.getCollectionNames();
            LOG.debug("Received {} collections", colls.size());
            Platform.runLater(() -> {
                collection.getItems().setAll(colls);
            });
            return null;
        }

        @Override protected void failed() {
            super.failed();
            Platform.runLater(() -> {
                database.getItems().clear();
                collection.getItems().clear();
            });
        }
    }
    
    private class FetchLogEventsTask extends MongoTask<Void> {

        public FetchLogEventsTask() {
            super("Error while fetching log events from Mongo", "Mongo Error", progress);
        }

        @Override protected Void call() throws Exception {
            LOG.debug("Fetching log events");
            coll = db.getCollection(collection.getValue());
            DBCursor c = coll.find();
            Set<String> levels = new HashSet<>();
            Set<String> markers = new HashSet<>();
            logs.clear();
            while (c.hasNext()) {
                LogEntry e = new LogEntry(c.next());
                logs.add(e);
                String l = e.getLevel().get();
                levels.add(l == null ? "" : l);
                String m = e.getMarker().get();
                markers.add(m == null ? "" : m);
            }
            LOG.debug("Received {} log events", logs.size());
            Platform.runLater(() -> {
                level.getItems().setAll(levels);
                marker.getItems().setAll(markers);
                level.getSelectionModel().selectAll();
                marker.getSelectionModel().selectAll();
                refreshTable();
            });
            return null;
        }

        @Override protected void failed() {
            super.failed();
            Platform.runLater(() -> {
                database.getItems().clear();
                collection.getItems().clear();
            });
        }
    }
}
