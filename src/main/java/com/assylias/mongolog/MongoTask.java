/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongolog;

import com.google.common.base.Throwables;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import org.controlsfx.dialog.Dialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public abstract class MongoTask<T> extends Task<T> {

    private static final Logger LOG = LoggerFactory.getLogger(MongoTask.class);

    private final String userMessageIfMongoError;
    private final String logMessageIfMongoError;
    private final ProgressIndicator progress;

    public MongoTask(String userMessageIfMongoError, String logMessageIfMongoError, ProgressIndicator progress) {
        this.userMessageIfMongoError = userMessageIfMongoError;
        this.logMessageIfMongoError = logMessageIfMongoError;
        this.progress = progress;
    }
    
    @Override protected void cancelled() {
        super.succeeded();
        progress.setVisible(false);
    }

    @Override protected void succeeded() {
        super.succeeded();
        progress.setVisible(false);
    }

    @Override protected void failed() {
        super.failed();
        progress.setVisible(false);
        Throwable t = getException();
        LOG.error(logMessageIfMongoError, t);
        Dialogs.create()
                .message(userMessageIfMongoError)
                .title("Mongo Error")
                .showException(t);
    }
}