/*
 * Copyright (C) 2011 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.mongolog;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.controlsfx.control.PropertySheet;
import org.controlsfx.dialog.Dialog;

public class Main {//extends Application {
//
//    @Override
//    public void start(Stage primaryStage) {
//        GridPane pane = new GridPane();
//        Button clickMe = new Button("Click me");
//        pane.add(clickMe, 0, 0);
//        Scene scene = new Scene(pane, 800, 600);
//        Dialog dlg = new Dialog(null, "I'm a dialog");
//
//        clickMe.setOnAction(a -> {
//            ObservableList<PropertySheet.Item> items = FXCollections.observableArrayList();
//            items.addAll(new NameItem());
//            PropertySheet ps = new PropertySheet(items);
//            ps.setModeSwitcherVisible(false);
//            ps.setSearchBoxVisible(false);
//            ps.setMode(PropertySheet.Mode.CATEGORY);
//            dlg.setContent(ps);
//            dlg.show();
//        });
//        primaryStage.setTitle("Hello World");
//        primaryStage.setScene(scene);
//        primaryStage.show();
//    }
//
//    public static void main(String[] args) {
//        launch(Main.class);
//    }
//
//    private class NameItem implements PropertySheet.Item {
//
//        @Override
//        public Class<?> getType() {
//            return String.class;
//        }
//
//        @Override
//        public String getCategory() {
//            return "";
//        }
//
//        @Override
//        public String getName() {
//            return "Name";
//        }
//
//        @Override
//        public String getDescription() {
//            return "Name";
//        }
//
//        @Override
//        public Object getValue() {
//            return "Rudolph";
//        }
//
//        @Override
//        public void setValue(Object o) {
//        }
//    }
}
