/*
 * Copyright (C) 2011 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.mongolog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mongodb.DBObject;
import java.util.Date;
import java.util.regex.Matcher;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Yann Le Tallec
 */
public class LogEntry {
    private static final String NEW_LINE = System.getProperty("line.separator");
    private static final Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
    private static final JsonParser jp = new JsonParser();

    private final StringProperty level;
    private final StringProperty marker;
    private final ObjectProperty<Date> date;
    private final StringProperty message;
    private final StringProperty details;

    public LogEntry(DBObject log) {
        level = new SimpleStringProperty((String) log.get("level"));
        date = new SimpleObjectProperty<>((Date) log.get("timestamp"));
        marker = new SimpleStringProperty((String) log.get("marker"));
        message = new SimpleStringProperty((String) log.get("message"));

        JsonElement je = jp.parse(log.toString());
        String fullMessage = gson.toJson(je);
        fullMessage = fullMessage.replaceAll("(\\\\r)?\\\\n", Matcher.quoteReplacement("\n")).replace("\\\"", "\"");
        
        details = new SimpleStringProperty(fullMessage);
    }
    
    public StringProperty getLevel() {
        return level;
    }
    public StringProperty getMarker() {
        return marker;
    }
    public ObjectProperty<Date> getDate() {
        return date;
    }
    public StringProperty getMessage() {
        return message;
    }
    public StringProperty getDetails() {
        return details;
    }

    @Override
    public String toString() {
        System.out.println(details.get());
        return details.get();
    }
    
}
