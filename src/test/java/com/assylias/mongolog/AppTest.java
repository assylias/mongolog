/*
 * Copyright (C) 2011 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.mongolog;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Yann Le Tallec
 */
public class AppTest {
    
    @Test
    public void testSomeMethod() {
        App.main();
    }
    
}
